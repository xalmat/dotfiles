# [XFCE] Red Robot

![Screenshot of Desktop](super-fighting-red-robot.png  "Super Fighting Red Robot")

A red theme for XFCE on Debian Stretch, featuring a Super Fighting Robot that happens to be Red. 

Wallpaper is modified from source. Not sure on the redistribution license, but it is easily reproducible.

# dotfiles and other info

Dotfiles and other files for my current XFCE-based desktop.

- [conky.conf](conky.conf) - my current conky config.
- [.gtkrc-2.0](gtkrc-2.0) - GTK2 dotfile, place in ~/
- [settings.ini](settings.ini) - GTK3 dotfile, place in ~/.config/gtk-3.0

# Links

Some of these may be found in various Linux Distro repositories too.

## Wallpaper

- [Mega Man Minimalist Wallpapers](https://www.reddit.com/r/wallpapers/comments/z4ywb/mega_man_minimalist_wallpapers_1920x1080/)  - Original source for the wallpaper. My version is modified to include the Debian logo on the shield.

## Fonts

- [Monoid](https://larsenwork.com/monoid/) - Source for Monoisome font
- [Nimbus Sans L](https://www.fontsquirrel.com/fonts/nimbus-sans-l) - WM Titlebar font

## Theme Files

- [Albatross](https://github.com/shimmerproject/Albatross) - WM Theme
- [Arc-Red-Darker](https://github.com/mclmza/arc-theme-Red) - GTK2/3 Theme
- [Papyrus-red](https://github.com/gembutterfly/papyrus-icon-theme) - Icon theme
- [Comix Cursor original Red Opaque](https://gitlab.com/limitland/comixcursors/) - Mouse Cursor